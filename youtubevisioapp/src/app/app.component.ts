import { Component , OnInit , DoCheck } from '@angular/core';
import { YoutubeServiceService } from './youtube-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit , DoCheck {
  urllist: Array<string> = []; // to save the url into the url list
  bookmarklist: Array<string> = []; // to save the url into the bookmark list
  reference: string = '';
  ngOnInit() {

    this.urllist = this.youtubeService.getListedesurlsFromdatabase();

  }

  ngDoCheck() {
    this.bookmarklist = this.youtubeService.bookmarklist;
    this.urllist = this.youtubeService.urllist;
    this.reference = this.youtubeService.getReference();
 }
  url: string='';
  state : boolean = false;
  title = 'Youtubevisio';
  constructor(private youtubeService: YoutubeServiceService ) { }



  setUrl(link: string ) {
    this.youtubeService.postToDatabase(link);
    this.state=false;
    this.url=link;
    this.youtubeService.setUrl(this.url);
    this.youtubeService.addUrl(this.url);
  }
  addToBookmark( link :string ) {
    this.youtubeService.addToBoockmarks(link);
  }
  showList( state : boolean ) {
    this.state=state;
  }

}
