import {Component, OnInit, Output, EventEmitter} from '@angular/core';


@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  constructor() { }
  link: string='';


  @Output() submitEventWatch=new EventEmitter<any>();
  @Output() submitEventAdd=new EventEmitter<any>();
  @Output() submitEventShow=new EventEmitter<any>();

  ngOnInit() {}

  onSubmit() {
    this.submitEventWatch.emit(this.link);
  }
  addToBookmarks() {
    this.submitEventAdd.emit(this.link);
  }

  showList() {
    this.submitEventShow.emit(true);
  }







}
