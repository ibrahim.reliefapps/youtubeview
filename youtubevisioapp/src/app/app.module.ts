import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { FormsModule} from '@angular/forms';
import { VideoViewComponent } from './video-view/video-view.component';
import { AppRoutingModule } from './/app-routing.module';
import { HistoyComponent } from './histoy/histoy.component';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    SearchBarComponent,
    VideoViewComponent,
    HistoyComponent,
    BookmarksComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
