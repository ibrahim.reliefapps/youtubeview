import {Component, Input, OnInit} from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-video-view',
  templateUrl: './video-view.component.html',
  styleUrls: ['./video-view.component.css']
})


export class VideoViewComponent implements OnInit {

  @Input() ref: string = '';

  constructor(private sanitizer: DomSanitizer ) {
  }
  ngOnInit() {}

  buildUrl() {
    return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'.concat(this.ref));
  }

}
